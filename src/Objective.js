import Collapsible from "react-collapsible";
import React from "react";

const titleObj = {en:'Objective',fr:'Objectif professionnel'}

const Objective = ({language}) => {

    const title  = language==='EN' ? titleObj.en : titleObj.fr
    return (
        <Collapsible trigger={title}>
            To be completed
        </Collapsible>
    );
};

export default Objective;