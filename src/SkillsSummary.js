import Collapsible from "react-collapsible";
import React from "react";

const titleObj = {en:'Skills Summary',fr:'Compétences'}

const SkillsSummary = ({language}) => {

    const title  = language==='EN' ? titleObj.en : titleObj.fr
    return (
        <Collapsible trigger={title}>
          To be completed
        </Collapsible>
    );
};

export default SkillsSummary;