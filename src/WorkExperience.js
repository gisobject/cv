import Collapsible from "react-collapsible";
import React from "react";

const titleObj = {en:'Work Experience',fr:'Expérience professionnelle'}

const WorkExperience = ({language}) => {

    const title  = language==='EN' ? titleObj.en : titleObj.fr
    return (
        <Collapsible trigger={title}>
            To be completed
        </Collapsible>
    );
};

export default WorkExperience;