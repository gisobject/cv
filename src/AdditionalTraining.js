import Collapsible from "react-collapsible";
import React from "react";

const titleObj = {en:'Additional Training',fr:'Formations complémentaires'}

const AdditionalTraining = ({language}) => {

    const title  = language==='EN' ? titleObj.en : titleObj.fr
    return (
        <Collapsible trigger={title}>
            To be completed
        </Collapsible>
    );
};

export default AdditionalTraining;